declare module '*.vue' {
  import Vue from 'vue';
  export default Vue;
}

// with this every module not covered by other typescript-definitions
// will have its export-type set to "any"
declare module '*';
